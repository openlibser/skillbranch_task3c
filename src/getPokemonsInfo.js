const cachingFetch = require('./cachingFetch');

async function getPokemonsNameAndUrl() {
  const firstPageURL = 'https://pokeapi.co/api/v2/pokemon';

  const pokemonCollector = async function inner(pokemons, pageUrl) {
    const pageResp = await cachingFetch(pageUrl);
    const pokemonsInfo = await pageResp.json();
    const nextPageUrl = pokemonsInfo.next;
    const pokemonsList = pokemonsInfo.results;
    const updatedPokemonsList = [...pokemons, ...pokemonsList];

    if (!nextPageUrl) {
      return updatedPokemonsList;
    }

    return await inner(updatedPokemonsList, nextPageUrl);
  };

  return await pokemonCollector([], firstPageURL);
}

async function getPokemonsInfo() {
  const pokNameAndURL = await getPokemonsNameAndUrl();
  const pokemonsPromises = pokNameAndURL.map(inf => cachingFetch(inf.url));
  const allPockemonsInfo = await Promise.all(pokemonsPromises);

  return allPockemonsInfo;
}

module.exports = getPokemonsInfo;
