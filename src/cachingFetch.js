const fetch = require('isomorphic-fetch');
const fs = require('fs');
const _ = require('lodash');

async function cachingFetch(url) {
  const cacheFileName = url.replace(/\//g, '_');
  const cacheFilePath = `./cache/${cacheFileName}`;
  // метод для обратной совместимости с оригинальным fetch
  const dummyJsonMethod = function () {
    return _.omit(this, ['json']);
  };

  if (fs.existsSync(cacheFilePath)) {
    const jsonStr = fs.readFileSync(cacheFilePath, 'utf8');
    const resBodyObj = JSON.parse(jsonStr);
    // для обратной совместимости с оригинальным fetch
    resBodyObj.json = dummyJsonMethod.bind(resBodyObj);
    return resBodyObj;
  }

  try {
    const response = await fetch(url);
    const resData = await response.json();
    fs.writeFileSync(cacheFilePath, JSON.stringify(resData, '', 2));
    // для обратной совместимости с оригинальным fetch
    resData.json = dummyJsonMethod.bind(resData);
    return new Promise(res => res(resData));
  } catch (err) {
    return new Promise((res, rej) => rej(err));
  }
}

module.exports = cachingFetch;
