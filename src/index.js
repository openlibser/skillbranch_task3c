const express = require('express');
const fs = require('fs');

const allowCrossDomain = require('./skillBranchCORS');

const app = express();

app.use(allowCrossDomain);

// папка с кешем в архиве pok_cache.zip
const pokemonsJSON = fs.readFileSync(__dirname + '/../cache/all_pokemons.json');
const pokemons = JSON.parse(pokemonsJSON);

const getOffsetLimit = (arr, offset = 0, limit = 20) =>
  arr.slice(+offset, +offset + +limit);

const getSortFunc = (ascOrDesc, sign) => (pok1, pok2) => {
  if (pok1[sign] === pok2[sign]) {
    return pok1.name > pok2.name ? 1 : -1; // Лексикографический порядок по имени
  }

  return ascOrDesc === 'asc'
    ? pok1[sign] - pok2[sign]
    : pok2[sign] - pok1[sign]; // desc
};

const getNamesSortedBySign = (sign, calcFunc, ascOrDesc) => {
  const namesAndSign = pokemons.map(pok => ({
    name: pok.name,
    [sign]: calcFunc(pok),
  }));

  const sortFunc = getSortFunc(ascOrDesc, sign);
  const sortedNames = namesAndSign.sort(sortFunc).map(pok => pok.name);

  return sortedNames;
};

const calcFuncsBySign = {
  angular: {
    f: pok => pok.weight / pok.height,
    order: 'asc',
  },
  fat: {
    f: pok => pok.weight / pok.height,
    order: 'desc',
  },
  huge: {
    f: pok => pok.height,
    order: 'desc',
  },
  micro: {
    f: pok => pok.height,
    order: 'asc',
  },
  light: {
    f: pok => pok.weight,
    order: 'asc',
  },
  heavy: {
    f: pok => pok.weight,
    order: 'desc',
  },
};

app.get('/', (req, res) => {
  const pokNames = pokemons.map(pok => pok.name).sort();
  const result = getOffsetLimit(pokNames, req.query.offset, req.query.limit);

  res.json(result);
});

app.get('/:sign', (req, res) => {
  const sign = req.params.sign;
  const calcFunc = calcFuncsBySign[sign] && calcFuncsBySign[sign].f;
  const order = calcFuncsBySign[sign] && calcFuncsBySign[sign].order;
  if (!calcFunc || !order) {
    throw new Error('404');
  }

  const sortedByAngular = getNamesSortedBySign(sign, calcFunc, order);
  const pokWithLimits = getOffsetLimit(sortedByAngular, req.query.offset, req.query.limit);

  res.json(pokWithLimits);
});

// routes error handling
app.use((err, req, res, next) => {
  if (err && err.message === '404') {
    res.sendStatus(404);
  } else if (err) {
    res.status(500).send('Server error.');
    console.error(err);
  }
  next();
});

app.listen(3000, () => console.log('Server runing on 3000 port'));
